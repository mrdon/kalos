#!/usr/bin/env python
import asyncio
import json
import logging
import nghttp2
import io
from werkzeug import exceptions
from werkzeug.routing import Map, Rule


class Application:

    def __init__(self):
        self.map = Map()
        self.routes = None
        """ :type : MapAdapter """

    def __getattr__(self, item):
        def route(path, **kwargs):
            def outer(func):
                self.map.add(Rule(path, endpoint=func, methods=[item], **kwargs))
                return func
            return outer
        return route

    @asyncio.coroutine
    def process(self, handler):
        print("in porcess")
        if self.routes is None:
            self.routes = self.map.bind("localhost")
        full_path = handler.path.decode('utf-8')
        scheme, netloc, path, params, query, frag = nghttp2.urlparse(full_path)

        method = handler.method.decode('utf-8')

        try:
            endpoint, args = self.routes.match(path_info=path, method=method, query_args=query)

            result = endpoint(handler, *args)
            if asyncio.iscoroutine(result):
                result = yield from endpoint(handler, **args)
            if isinstance(result, str):
                handler.send_response(200, body=io.BytesIO(result.encode('utf-8')))
        except exceptions.HTTPException as e:
            handler.send_response(status=e.code, body=e.description.encode('utf-8'))
        except Exception as e:
            logging.exception(e)

    def run(self, host='127.0.0.1', port=8443, ssl_context=None, serve_forever=True):
        print("HTTP/2 server on {}:{}".format(host, port))
        import logging
        import sys
        logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

        server = nghttp2.HTTP2Server((host, port), lambda *args, **kwargs: Handler(self, *args, **kwargs), ssl=ssl_context)
        if serve_forever:
            asyncio.get_event_loop().run_forever()
        return server


class Handler(nghttp2.BaseRequestHandler):

    def __init__(self, app, *args, **kwargs):
        super().__init__(*args, **kwargs)
        print("creating handler")
        self.app = app
        self.body = asyncio.Queue()
        self.body_done = asyncio.Future()
        self.request_done = asyncio.Future()
        self.closed_cb = asyncio.Future()

    def on_headers(self):
        print("eahasdfers")
        task = asyncio.async(self.app.process(self))

        def on_close(_):
            if not task.done():
                task.cancel()
        self.closed_cb.add_done_callback(on_close)

    def on_data(self, data):
        self.body.put_nowait(data)

    def on_request_done(self):
        self.body_done.set_result(self.body)

    def on_close(self, error_code):
        self.closed_cb.set_result(error_code)

    def closed(self):
        return self.closed_cb.done()

    @asyncio.coroutine
    def json(self):
        data_str = yield from self.text()
        if data_str:
            return json.loads(data_str)
        else:
            return None

    @asyncio.coroutine
    def text(self):
        yield from self.body_done
        if not self.body.empty():
            txt = "".join(x.decode('utf-8') for x in __iter_except(self.body.get_nowait, asyncio.QueueEmpty))
            return txt

    def push_stream_from_queue(self, path, queue, transformer=None, status=200, request_headers=None, headers=None,
                               method='GET'):
        feeder = self._deliver_stream_from_queue(
            queue=queue,
            transformer=transformer,
            deliverer=lambda body_func: self.push(path, status=status, method=method, headers=headers,
                                                  request_headers=request_headers, body=body_func)
        )
        return asyncio.async(feeder())

    @asyncio.coroutine
    def send_stream_from_queue(self, queue, transformer=None, status=200, headers=None):
        feeder = self._deliver_stream_from_queue(
            queue=queue,
            transformer=transformer,
            deliverer=lambda body_func: self.send_response(status=status, headers=headers, body=body_func)
        )
        yield from feeder()

    def _deliver_stream_from_queue(self, deliverer, queue, transformer=None):
        item_list = []

        def items_response(length):
            if not item_list:
                return None, nghttp2.DATA_DEFERRED
    
            item = item_list.pop()

            transformed = item if not transformer else transformer(item)

            if isinstance(transformed, str):
                transformed = transformed.encode('utf-8')

            if len(transformed) > length:
                raise ValueError("Length too long: {}".format(len(transformed)))

            return transformed, nghttp2.DATA_OK

        handler = deliverer(items_response)
        if handler is None:
            handler = self

        @asyncio.coroutine
        def feed():
            while not self.closed():

                # prior to 3.5, wait_for doesn't cancel so we have to do all this
                reader = asyncio.async(asyncio.wait_for(queue.get(), 2))
                try:
                    msg = (yield from reader)
                except asyncio.TimeoutError:
                    reader.cancel()
                    continue

                if handler.closed():
                    break
                if msg:
                    item_list.append(msg)
                    handler.resume()
        return feed


def __iter_except(func, exception, first=None):
    """ Call a function repeatedly until an exception is raised."""
    try:
        if first is not None:
            yield first()            # For database APIs needing an initial cast to db.first()
        while 1:
            yield func()
    except exception:
        pass
