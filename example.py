import kalos
import asyncio

app = kalos.Application()

@app.get('/foo')
def stream(request):
    print("got get")
    return "hi friend"

if __name__ == "__main__":
    app.run(host='0.0.0.0')
