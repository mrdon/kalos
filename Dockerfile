FROM mrdonbrown/nghttp2-py3

MAINTAINER Don Brown <https://bitbucket.org/mrdon/kalos>

WORKDIR /code

ADD requirements.txt /code/

RUN pip3 install -r /code/requirements.txt

ADD . /code

EXPOSE 8443 

CMD ["python3", "example.py"]



